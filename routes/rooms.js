let Room = require("../models/rooms")
let mongoose = require("mongoose")
let express = require("express")
let router = express.Router()
mongoose.connect("mongodb://wang:wfb123@ds343217.mlab.com:43217/heroku_rrll436g")
let db = mongoose.connection


db.on("error", function (err) {
    console.log("Unable to Connect to [ " + db.name + " ]", err)
})

db.once("open", function () {
    console.log("Successfully Connected to [ " + db.name + " ]")
})


router.findAll = (req, res) => {
    Room.find(function(err, rooms) {
        if (err)
            res.send(err)
        else { 
            // console.log(donations)
            res.send(rooms)
        }
    })
}
router.findByRoomNumber = (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    Room.find({ "roomNumber" : req.params.roomNumber },function(err, room) {
        if (err)
            res.send({ message: "Room NOT Found!", errmsg : err } )
        else
            res.send(room)
    })
}
router.changeAvailable = (req, res) => {
    Room.update({"roomNumber":req.params.roomNumber},{$set:{"available":req.body.available}},function(err){
        if (err)
            res.send({ message: 'Room NOT changed!', errmsg : err } );
        else
            res.send({ message: 'Room Successfully changed!'});
    })
}

router.addRoom = (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    let room = new Room()

    room.roomtype = req.body.roomtype
    room.price = req.body.price
    room.roomNumber = req.body.roomNumber
    room.available = req.body.available
    room.save(function(err) {
        if (err)
            res.send({message:'failed', errmsg: err});// return a suitable error message
        else
            res.send({ message: 'Room Successfully changed!'});// return a suitable success message
    })
}
router.deleteRoom = (req, res) => {
    Room.remove({ "roomNumber" : req.params.roomNumber }, function(err) {
        if (err)
            res.send(err)
        else
            res.send({ message: "Room Deleted!", data: Room})
    })
}
router.login = (req, res ) => {
    res.setHeader('Content-Type', 'application/json');
    User.find( {"username" : req.body.username}, function(err, user){
        if(user.length === 0)
            res.send("user doesn't exist")
        else {
            if(user[0].password == req.body.password){ res.send("welcome") }
            else { res.send("password incorrect") }
        }
    })
}

router.register =(req, res) => {
    let username = req.body.username
    let password = req.body.password

    User.find ( {"username" : username}, function (err, user) {
        if(user.length === 0) {
            //res.send("you can use this name")
            let newuser = new User();

            newuser.username = username;// the requested value
            newuser.password = password;// the requested value

            newuser.save(function (err) {
                if (err)
                    res.json({message: 'failed', errmsg: err});// return a suitable error message
                else
                    res.send("user create successfully");// return a suitable success message
            });
        }else{
            res.send("user already exist")
        }
    })
}
router.getauser = (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    User.find({username : req.params.username} ,function(err, user) {
        if (user.length === 0)
            res.send({ message: 'NOT Found!'} )
        else {
            res.send(user)
        }
    });
}

module.exports = router