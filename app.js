/*eslint no-unused-vars: "off" */
let createError = require("http-errors")
let express = require("express")
let path = require("path")
let cookieParser = require("cookie-parser")
let logger = require("morgan")
let cors = require("cors")

let indexRouter = require("./routes/index")
let usersRouter = require("./routes/users")

const rooms = require("./routes/rooms")

let app = express()

// view engine setup
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")

app.use(logger("dev"))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public")))
app.use(cors())

app.use("/", indexRouter)
app.use("/users", usersRouter)

app.get("/rooms", rooms.findAll)
app.get("/rooms/:roomNumber", rooms.findByRoomNumber)

app.put("/rooms/:roomNumber",rooms.changeAvailable)

app.post("/rooms",rooms.addRoom)

app.delete("/rooms/:roomNumber", rooms.deleteRoom)
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get("env") === "development" ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.render("error")
})

module.exports = app
