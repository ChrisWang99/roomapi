Name: Fubo Wang
Student ID: 20086458

Brief description of functionality: 
This application is part of the hotel management system. The hotel can use this app to manage hotel room information. This app provides query, modify, add, and delete features.

App Features (all via RESTful API): 
	POST a room type, a price, a room number and available in JSON format
	GET a list of rooms
	GET a room using a room number
	DELETE a room using an roomNumber
	PUT room’s available

Persistence approach adopted: 
The data for this application is stored in mlab. This app was deployed to heroku. Sourcetree is used during deployment.

Link to git project / access: 
https://gitlab.com/ChrisWang99/roomapi.git


